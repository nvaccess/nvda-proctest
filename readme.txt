Specific Process Automated Testing Environment for NVDA

This environment allows NVDA to be used for automated testing with a specific process. It is configured to run without speech or braille, log input/output, etc. It also includes a plugin to make NVDA watch events only from a specific process.

Preparation:
1. Obtain a portable copy of NVDA.
2. Place this environment in a directory accessible to NVDA.

Usage:
1. Start the application being tested.
2. Set the NVDA_SPECIFIC_PROCESS environment variable to the process id of the application being tested.
3. Run NVDA using the following command, where envPath is the path to this environment and logPath is the desired path to the NVDA log file: nvda_noUIAccess.exe -m -c envPath -f logPath
4. To exit NVDA, run: nvda_noUIAccess.exe -q
