"""Make NVDA only handle events from a specific process.
This is useful for automated testing of a specific application.
To use, set the NVDA_SPECIFIC_PROCESS environment variable to the process id of
the process you're interested in, then start NVDA using nvda_noUIAccess.exe.
nvda_noUIAccess.exe has to be used because elevation loses the environment.
@author: James Teh <jamie@nvaccess.org>
@copyright: 2014 NV Access Limited
"""

import os
import globalPluginHandler
import eventHandler
import api

class GlobalPlugin(globalPluginHandler.GlobalPlugin):

	def __init__(self):
		super(GlobalPlugin, self).__init__()
		self.pid = None
		try:
			self.pid = int(os.getenv("NVDA_SPECIFIC_PROCESS"))
		except (TypeError, ValueError):
			return
		if not self.pid:
			return
		# Monkey patch eventHandler.queueEvent.
		self._origQueueEvent = eventHandler.queueEvent
		eventHandler.queueEvent = self._queueEvent

	def terminate(self):
		if self.pid:
			eventHandler.queueEvent = self._origQueueEvent
			self._origQueueEvent = None
		super(GlobalPlugin, self).terminate()

	def _queueEvent(self, eventName, obj, **kwargs):
		if obj.processID != self.pid:
			if eventName == "gainFocus":
				# Make NVDA sleep while another process is focused.
				api.getFocusObject().sleepMode = True
				# If NVDA's current focus gets focus again when switching back to the target process,
				# NVDA mustn't filter it out it as a duplicate.
				eventHandler.lastQueuedFocusObject = obj
			return
		self._origQueueEvent(eventName, obj, **kwargs)
